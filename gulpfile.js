// Imports

const gulp         = require('gulp'),
      del          = require('del'),
      rename       = require('gulp-rename'),
      // Templating
      nunjucks     = require('gulp-nunjucks-render'),
      htmlmin      = require('gulp-htmlmin'),
      // CMS
      data = require('gulp-data'),
      Techy = require('techy-cms').gulp({root: 'articles/'}),
      frontMatter = require('front-matter'),
      // CSS processing
      purgecss     = require('gulp-purgecss'),
      postcss      = require('gulp-postcss'),
      tailwindcss  = require('tailwindcss'),
      autoprefixer = require('autoprefixer'),
      cleanCSS     = require('gulp-clean-css'),
      // JavaScript
      webpack      = require('webpack-stream'),
      // BrowserSync
      browserSync  = require('browser-sync'),
      reload       = browserSync.reload;


// TASKS

// Clean previous build

gulp.task('clean', function(done) {
  // Deletes all files from dist/
  del.sync('dist/', {force: true});
  del.sync(['src/news/*', '!src/news/index.html']);
  done();
});


// Reload the browser

gulp.task('reload', function(done){
  reload();
  done();
});


// Nunjucks

gulp.task('nunjucks', function() {
  // Get all .html files in pages
  return gulp.src('src/**/*.html')
  // Adds data from downloads.json
  .pipe(data(function() {
    return require('./src/data/downloads.json')
  }))
  // Render template with nunjucks
  .pipe(nunjucks({
    path: ['src/templates/']
  }))
  .pipe(htmlmin({ // minify HTML
    collapseWhitespace: true,
    removeComments: true
  }))
  // Output files in dist folder
  .pipe(gulp.dest('dist'))
});


// CSS processing

function TailwindExtractor(content) {
  // Extract tailwindcss classes
  // https://tailwindcss.com/docs/optimizing-for-production#writing-purgeable-html
  return content.match(/[^<>"'`\s]*[^<>"'`\s:]/g);
}

// Process CSS to be as small as possible with postCSS and purgeCSS
// This task takes a few seconds - only use it in production
gulp.task('css-prod', function() {
  return gulp.src('src/css/style.css')
    // postcss
    .pipe(postcss([
      tailwindcss,
      autoprefixer
    ]))
    // purgecss
    .pipe(purgecss({
      content: ['src/**/*.html', 'src/**/*.njk', 'src/**/*.svg', 'articles/_tpl/*.html'],
      extractors: [{
        extractor: TailwindExtractor,
        extensions: ['html', 'njk', 'svg']
      }]
    }))
    // Minifies CSS
    .pipe(cleanCSS({compatibility: 'ie9'}))
    // output files in dist folder
    .pipe(gulp.dest('dist/static/css/'));
});

// Use the complete tailwind.min.css file for development only
gulp.task('css-dev', function() {
  return gulp.src('src/css/style.css')
    // postcss
    .pipe(postcss([
      tailwindcss
    ]))
    // output files in dist folder
    .pipe(gulp.dest('dist/static/css/'));
});


// Bundle and minify JavaScript
gulp.task('js', () =>
  gulp.src('src/js/app.js')
    .pipe(webpack({mode: 'production'}))
    .pipe(gulp.dest('dist/static/js/'))
)


// Copy all static files

gulp.task('copy-static', function(done){
  gulp.src('src/static/**/*').pipe(gulp.dest('dist/static/'));
  done();
});


// Start browserSync

gulp.task('serve', function(done){
  browserSync({
    server: {
      baseDir: './dist',
      index: "index.html",
      serveStaticOptions: {
        extensions: ['html']
      }
    }
  });
  done();
});


// Techy CMS

gulp.task('techy', function(done){
  gulp.src('articles/**/*.md')
    .pipe(Techy())
    .pipe(gulp.dest('./src/news/'));
  done();
});

function sortByProperty(property) {
  return function(a,b){
   if(a[property] < b[property])
     return 1;
   else if(a[property] > b[property])
     return -1;
   return 0;
  }
 }

gulp.task('generateIndex', function(done) {
  // List all files in the blog directory
  const files = fs.readdirSync('./src/news/');  // all blog posts
  const blogsArray = 'src/templates/partials/articles.njk';

  const prepend = '{% set articles = [';
  const append = '{}] %}';

  fs.writeFileSync(blogsArray, prepend, function (err, data) {
    if (err) {return console.log(err);}
  });

  // files object contains all files names
  var index = 0;
  var indexArray = [];
  files.forEach(filename => {
    // Trim the .html extenstion
    filename = filename.slice(0, -5);
    let tmpData = '';  // temporary string to hold the json of each post

    // Read the Front Matter
    if (filename !== 'index'){
      var data = fs.readFileSync('articles/' + filename + '.md', 'utf-8');
      // Append the URL
      tmpData += `{"url": "${filename}",`

      const fm = frontMatter(data);

      var tags = "[";
      for (var i = 0; i < fm.attributes.tags.length; i++) {
        let end = '",';
        if (i === fm.attributes.tags.length - 1) {
          end = '"';
        }
        tags += '"' + fm.attributes.tags[i] + end;
      }
      tags += "]";

      // Populate the JSON array
      tmpData += `"id": "${fm.attributes.id}",
        "title": "${fm.attributes.title}",
        "date": "${fm.attributes.date}",
        "author": "${fm.attributes.author}",
        "image": "${fm.attributes.image}",
        "tags": ${tags},
        "draft": ${fm.attributes.draft},
        "desc": "'${fm.attributes.description}"}`;

      // Increment the index
      index++;
    }

    if (tmpData)
      indexArray.push(JSON.parse(tmpData));

  });

  const sortedJSONArray = indexArray.sort(sortByProperty('id'));
  const sortedStringArray = [];
  for (item in sortedJSONArray) {
    sortedStringArray.push(JSON.stringify(sortedJSONArray[item]));
  }

  for (item in sortedStringArray) {
    let tmpText = sortedStringArray[item] + ',';
    fs.appendFileSync(blogsArray, tmpText, function (err, data) {
      if (err) {return console.log(err);}
    });
  }

  fs.appendFileSync(blogsArray, append, function (err, data) {
    if (err) {return console.log(err);}
  });

  done();
});


// Watch for changes

gulp.task('watch', function(done){
  // Watch HTML pages
  gulp.watch('src/**/*',          gulp.series('nunjucks', 'css-dev', 'reload'));
  // Watch CSS files
  gulp.watch('src/css/**/*.css',  gulp.series('css-dev'));
  // Watch Techy CMS files
  gulp.watch('articles/**/*',     gulp.series('techy', 'nunjucks'));
  // Watch JS files
  gulp.watch('src/js/**/*',       gulp.series('js', 'reload'));
  // Watch static files
  gulp.watch('src/static/**/*.*', gulp.series('copy-static', 'reload'));
  done();
});


// Series

// Default task
gulp.task('default', gulp.series('clean', 'techy', 'css-dev', 'generateIndex',
  'nunjucks', 'js', 'copy-static', 'serve', 'watch'));

// Deployment task
gulp.task('build', gulp.series('clean', 'techy', 'css-prod', 'generateIndex',
  'nunjucks', 'js', 'copy-static'));
