---
layout: layout.html
id: '007'
title: "$10,000 Bounty for an Open-source GPU Mining Software Provided By B.U."
draft: false
description: Today, B.U. announces a bounty equivalent to US$10,000 (paid in BTC or
  BCH) for fully functional and fully open-source GPU mining software made free to
  the community.
date: 21st November 2022
image: "/static/articles/adi-goldstein-eusvweosble-unsplash-1.jpg"
author: Bitcoin Unlimited
tags:
- Software
- Mining
- Bounty

---
Bitcoin Unlimited is committed to decentralization in the Nexa mining network. The team wants to support and encourage a free market in mining software innovation (as aligned with the goals of our PoW algorithm). It is an excellent signal that at least one or more entities have been willing to commit their own resources to developing GPU mining software to get ahead of the game, It’s also important that mining is widely deployed and accessible to everyone. B.U. already supplies nexa-miner with the full node software, but its CPU capability has reached the end of its competitiveness.

Today, B.U. announces a bounty equivalent to US$10,000 (paid in BTC or BCH) for fully functional and fully open-source GPU mining software made free to the community. The bounty will be paid to the owner of the GitLab account which provides a _merge request_ with MIT license, fully functional and fully open-source code to upgrade nexa-miner.

At a minimum the software should detect the GPU(s) present in the hardware and fully utilize the cards by running an equivalent of the CpuMineBlock() function entirely in the GPU, using OpenCL.

The mining software must reach at least 3MH/s on an Nvidia 3080 graphics card.

Upon acceptance of the MR, by B.U. for our nexa-miner, after code review for performance, quality and security, the bounty will be paid. Should multiple teams produce working code, the Developer (with the advice of the rest of the BU team) will choose one based on quality and performance, with priority consideration to the date when the solution was submitted. If no submissions have happened by Dec 31, 2022 then at that time B.U. will decide whether to continue, expand, or discontinue this bounty.

Good luck and let’s make Nexa stronger through both competition and cooperation!

For any questions, ask in our [https://discord.gg/aKeeeTdC4Z](https://discord.gg/aKeeeTdC4Z ).